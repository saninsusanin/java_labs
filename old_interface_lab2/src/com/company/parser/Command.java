package com.company.parser;

public enum Command {
    INPUT,
    OUTPUT,
    CODES,
    WORKERS,
    CONFIGS,
    BUFFER_SIZE,
    MODE,
    CONFIGURATION
}
