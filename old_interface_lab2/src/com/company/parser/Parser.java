package com.company.parser;

import ru.spbstu.pipeline.logging.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Parser {
    private final static char separator;
    private final static char comment;
    private final static char space;
    private final static char enter;
    private final static int eof;
    private final static int dec;
    private final static int FIRST_VALUE;
    private final static HashMap<String, Command> commands;
    private final static HashMap<String, Mode> modes;
    private final Parameters parameters;
    private boolean fileIsEnded;
    private boolean isErrorOccurred = false;
    private Logger logger;


    static {
        separator = '=';
        comment = '#';
        space = ' ';
        enter = '\n';
        eof = -1;
        dec = 10;
        FIRST_VALUE = 0;

        modes = new HashMap<>();
        modes.put("ENCODE", Mode.ENCODE);
        modes.put("DECODE", Mode.DECODE);

        commands = new HashMap<>();
        commands.put("INPUT_PATH", Command.INPUT);
        commands.put("OUTPUT_PATH", Command.OUTPUT);
        commands.put("CODES_PATH", Command.CODES);
        commands.put("WORKER", Command.WORKERS);
        commands.put("CONFIG", Command.CONFIGS);
        commands.put("BUFFER_SIZE", Command.BUFFER_SIZE);
        commands.put("MODE", Command.MODE);
        commands.put("CODES", Command.CODES);
        commands.put("CONFIGURATION", Command.CONFIGURATION);
    }


    {
        parameters = new Parameters();
        fileIsEnded = false;
    }


    public Parser(Logger logger) {
        this.logger = logger;
    }


    static class DataFromLine {
        private String parameterName;
        private String parameterValue;

        public void setParameterName(String parameterName) {
            this.parameterName = parameterName;
        }

        public void setParameterValue(String parameterValue) {
            this.parameterValue = parameterValue;
        }

        public String getParameterName() {
            return parameterName;
        }

        public String getParameterValue() {
            return parameterValue;
        }
    }


    private int skipAllWhitespaces(BufferedReader input, int currentCharacter) throws IOException {

        while ((currentCharacter == enter || currentCharacter == space) && currentCharacter != eof) {
            currentCharacter = input.read();
        }

        return currentCharacter;
    }


    private int getEssential(StringBuilder currentString, BufferedReader input, int currentCharacter) throws IOException {

        while (currentCharacter != separator && currentCharacter != eof &&
                currentCharacter != enter && currentCharacter != space) {
            currentString.append((char) currentCharacter);
            currentCharacter = input.read();
        }

        return currentCharacter;
    }


    private DataFromLine lineParser(BufferedReader input, String pathToConfig) throws IOException {
        int character;

        StringBuilder parameterName = new StringBuilder();
        StringBuilder parameterValue = new StringBuilder();
        DataFromLine data = new DataFromLine();

        character = input.read();
        character = skipAllWhitespaces(input, character);

        if (character == comment) {
            data.setParameterName(new String(parameterName));
            data.setParameterValue(new String(parameterValue));

            while (true) {
                character = input.read();

                if (character == eof) {
                    fileIsEnded = true;
                    return data;
                } else if(character == enter){
                    return data;
                }

            }

        } else {
            character = getEssential(parameterName, input, character);
            character = skipAllWhitespaces(input, character);

            if (character != separator) {
                logger.log("Error in file " + pathToConfig + ". You have to put = between command name and command value");
                isErrorOccurred = true;

                while (input.read() != '\n');

                data.setParameterName(new String(parameterName));
                data.setParameterValue(new String(parameterValue));

                return data;
            }

            character = input.read();
            character = skipAllWhitespaces(input, character);
            character = getEssential(parameterValue, input, character);

            if (character == eof) {
                fileIsEnded = true;
            }
        }

        data.setParameterName(new String(parameterName));
        data.setParameterValue(new String(parameterValue));
        return data;
    }


    public Parameters ParseConfig(String pathToConfig) {
        BufferedReader config;

        try {
            config = new BufferedReader(new FileReader(new File(pathToConfig)));

            while (!fileIsEnded) {
                DataFromLine data = lineParser(config, pathToConfig);

                if (data.parameterName.length() == 0 && data.getParameterValue().length() == 0) {

                    continue;
                } else if (data.getParameterName().length() == 0 && data.getParameterValue().length() != 0) {
                    logger.log("No parameter name for value - " + data.getParameterValue());
                    isErrorOccurred = true;

                    break;
                } else if (data.getParameterName().length() != 0 && data.getParameterValue().length() == 0) {
                    logger.log("No value for parameter - " + data.getParameterName());
                    isErrorOccurred = true;

                    break;
                }

                Command current_command = commands.get(data.parameterName);

                switch (current_command) {
                    case BUFFER_SIZE:

                        if (Integer.parseInt(data.parameterValue, dec) % 2 != 0) {
                            logger.log("Because of using UTF-16BE encoding, all buffer size must be even");
                            isErrorOccurred = true;

                            break;
                        }

                    case INPUT:
                    case OUTPUT:
                    case CODES:
                    case MODE:

                        if (parameters.getParameter(current_command) != null) {
                            logger.log(data.parameterName + " is already set to - " + parameters.getParameter(current_command).get(FIRST_VALUE));
                            isErrorOccurred = true;
                        } else {
                            parameters.setParameter(current_command, data.parameterValue);
                        }

                        break;
                    case WORKERS:
                    case CONFIGS:
                        parameters.setParameter(current_command, data.parameterValue);

                        break;
                    default:
                        logger.log("No such command - " + data.getParameterName());
                        isErrorOccurred = true;

                        break;
                }

            }

            config.close();
        } catch (IOException e) {
            logger.log("Something went wrong with file " + pathToConfig);
            isErrorOccurred = true;
        }

        return parameters;
    }


    public boolean getStatus() {
        return isErrorOccurred;
    }
}
