package com.company.coder;

import com.company.parser.*;
import ru.spbstu.pipeline.logging.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Huffman {
    private final String codes;
    private final Mode mode;
    private Map<Character, Integer> frequencies;
    private Map<String, Character> codesCharacters;
    private Map<Character, String> charactersCodes;
    private final static int eof;
    private final static int bin;
    private final static int FIRST_VALUE;
    private final static String FALSE;
    private final static String TRUE;
    private final static String EMPTY_STRING;
    private final static HashMap<String, Mode> modes;
    private final static char separator;
    private final static char enter;
    private boolean isErrorOccurred;
    private Logger logger;


    static {
        eof = -1;
        bin = 2;
        FIRST_VALUE = 0;
        FALSE = "0";
        TRUE = "1";
        EMPTY_STRING = "";
        separator = '=';
        enter = '\n';

        modes = new HashMap<>();
        modes.put("ENCODE", Mode.ENCODE);
        modes.put("DECODE", Mode.DECODE);
    }


    public Huffman(Parameters parameters, Logger logger) {
        codes = parameters.getParameter(Command.CODES).get(FIRST_VALUE);
        mode = modes.get(parameters.getParameter(Command.MODE).get(FIRST_VALUE));
        this.logger = logger;
        isErrorOccurred = false;

        switch (mode) {
            case DECODE:
            case ENCODE:

                if (codes.length() == 0) {
                    logger.log("Set path to the codes file in the config file");
                    isErrorOccurred = true;
                }

                break;
            default:
                logger.log("No such mode - " + parameters.getParameter(Command.MODE).get(FIRST_VALUE));
                isErrorOccurred = true;

                break;
        }

    }


    private static class Node implements Comparable<Node> {
        final Integer sum;
        final Character character;
        final Node left;
        final Node right;

        public Node(Character character, Integer sum, Node left, Node right) {
            this.sum = sum;
            this.character = character;
            this.left = left;
            this.right = right;
        }

        @Override
        public int compareTo(Node rightOperand) {
            return Integer.compare(sum, rightOperand.sum);
        }

    }


    private void getFrequencies(byte[] data) {
        frequencies = new HashMap<>();

        for (int i = 0; i < data.length; ++i) {
            char currentSymbol = (char)(data[i]);

            if (!frequencies.containsKey(currentSymbol)) {
                frequencies.put(currentSymbol, 0);
            }

            frequencies.put(currentSymbol, frequencies.get(currentSymbol) + 1);
        }

    }


    private void setCodes(Node currentNode, String currentCode) {

        if (currentNode.left == null && currentNode.right == null) {
            codesCharacters.put(currentCode, currentNode.character);
            charactersCodes.put(currentNode.character, currentCode);
        }

        if (currentNode.left != null) {
            setCodes(currentNode.left, currentCode + FALSE);
        }

        if (currentNode.right != null) {
            setCodes(currentNode.right, currentCode + TRUE);
        }

    }


    private void fillCodes() {
        FileOutputStream codes;

        try {
            codes = new FileOutputStream(this.codes);

            for (Map.Entry<Character, String> entry : charactersCodes.entrySet()) {
                codes.write((byte)((char)entry.getKey()));
                codes.write((byte) separator);
                codes.write(Integer.parseInt(entry.getValue(), bin));
                codes.write((byte) enter);
            }

            codes.close();
        }
        catch(IOException e) {
            logger.log("Something went wrong with " + this.codes);
            isErrorOccurred = true;
        }

    }


    public String encode(byte[] data) {

        getFrequencies(data);

        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();

        for (Map.Entry<Character, Integer> entry : frequencies.entrySet()) {
            priorityQueue.add(new Node(entry.getKey(), entry.getValue(), null, null));
        }

        while (priorityQueue.size() > 1) {
            Node firstNode = priorityQueue.poll();
            Node secondNode = priorityQueue.poll();
            priorityQueue.add(new Node('\0', firstNode.sum + secondNode.sum, firstNode, secondNode));
        }

        Node root = priorityQueue.poll();
        codesCharacters = new HashMap<>();
        charactersCodes = new HashMap<>();

        if (root != null) {

            if (root.left == null && root.right == null) {
                codesCharacters.put(FALSE, root.character);
                charactersCodes.put(root.character, FALSE);
            } else {
                setCodes(root, EMPTY_STRING);
            }

            fillCodes();
        }

        StringBuilder output = new StringBuilder();

        for (int i = 0; i < data.length; ++i) {
            output.append(Integer.parseInt(charactersCodes.get((char)data[i]), bin));
        }

        return output.toString();
    }


    private void getCodes() throws IOException {
        char symbol;

        codesCharacters = new HashMap<>();
        charactersCodes = new HashMap<>();

        byte[] bt = new byte[1];
        FileInputStream codes = new FileInputStream(this.codes);

        while (codes.read(bt) != eof) {
            symbol = (char)bt[0];
            codes.read(bt);
            codes.read(bt);
            String code = Integer.toBinaryString(bt[0]);
            codes.read(bt);

            charactersCodes.put(symbol, code);
            codesCharacters.put(code, symbol);
        }
    }


    public String decode(byte[] data) {
        try {
            getCodes();
        } catch (IOException e) {
            logger.log("Error occurred while reading file " + codes);
            isErrorOccurred = true;

            return new String(data, StandardCharsets.US_ASCII);
        }

        StringBuilder output = new StringBuilder();
        StringBuilder currentCode = new StringBuilder();

        for (byte datum : data) {
            currentCode.append((char) datum);

            if (codesCharacters.get(currentCode.toString()) != null) {
                output.append(codesCharacters.get(currentCode.toString()));
                currentCode = new StringBuilder();
            }

        }

        if (currentCode.toString().length() != 0) {
            logger.log("No such code. Maybe your file wasn't encoded with this encoder");
            isErrorOccurred = true;

        }

        return output.toString();
    }


    public boolean getStatus() {
        return isErrorOccurred;
    }


    public Mode getMode() { return mode; }
}
