package com.company.workers;

import com.company.parser.Command;
import com.company.parser.Parameters;
import com.company.parser.Parser;
import org.jetbrains.annotations.NotNull;
import ru.spbstu.pipeline.Consumer;
import ru.spbstu.pipeline.Reader;
import ru.spbstu.pipeline.Status;
import ru.spbstu.pipeline.logging.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MyReader implements Reader {
    private final Logger logger;
    private int bufferSize;
    private String inputFilePath;
    private boolean isEnded;
    private byte[] data;
    private InputStream input;
    private ArrayList<Consumer> consumers;
    private Status status;

    private final static int FIRST_VALUE = 0;
    private final static int dec = 10;
    private final static int eof = -1;


    public MyReader(@NotNull String confFilePath, @NotNull Logger logger)  {
        this.logger = logger;
        consumers = new ArrayList<>();
        status = Status.OK;
        
        Parser parser = new Parser(logger);
        Parameters parameters = parser.ParseConfig(confFilePath);
        inputFilePath = parameters.getParameter(Command.INPUT).get(FIRST_VALUE);
        String bufferSizeString = parameters.getParameter(Command.BUFFER_SIZE).get(FIRST_VALUE);

        try {
            bufferSize = Integer.parseInt(bufferSizeString, dec);
        } catch (NumberFormatException e) {
            logger.log("Can't be parsed buffer size which is set to - " + bufferSizeString + " Buffer size has to be a even number");
            status = Status.READER_ERROR;
        }

        data = new byte[bufferSize];

        try {
            input = new FileInputStream(inputFilePath);
        } catch (FileNotFoundException e) {
            logger.log("Can't open file " + inputFilePath);
            status = Status.READER_ERROR;
        }

    }


    public void read() {
        try {
            int size = input.read(data);

            if (size == eof) {
                isEnded = true;
            } else if (size != bufferSize) {
                byte[] temporary = new byte[size];
                System.arraycopy(data, 0, temporary, 0, size);
                data = temporary;
            }

        } catch (FileNotFoundException e) {
            logger.log("File - " + inputFilePath + " doesn't exist or can't be opened for reading");
            status = Status.READER_ERROR;
        } catch (IOException e) {
            logger.log("Can't read data from file - " + inputFilePath);
            status = Status.READER_ERROR;
        }
    }


    public @NotNull byte[] get() {
        return data.clone();
    }


    public boolean isEnded() {
        return isEnded;
    }


    public void close() {

        try {
            input.close();
        } catch (IOException e) {
            logger.log("Can't close file " + inputFilePath);
        }

    }


    @Override
    public void run() {
        read();

        while (!isEnded) {

            for (Consumer consumer : consumers) {
                consumer.run();
            }

            read();
        }

    }


    @Override
    public void addConsumer(@NotNull Consumer consumer) {
        consumers.add(consumer);
    }


    @Override
    public void addConsumers(@NotNull List<Consumer> list) {

        for (Consumer consumer : list) {
            addConsumer(consumer);
        }

    }


    public Status getStatus() {
        return status;
    }
}
