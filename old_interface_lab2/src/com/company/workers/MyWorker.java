package com.company.workers;

import com.company.coder.Huffman;
import ru.spbstu.pipeline.logging.Logger;
import com.company.parser.Parameters;
import com.company.parser.Parser;
import org.jetbrains.annotations.NotNull;
import ru.spbstu.pipeline.Consumer;
import ru.spbstu.pipeline.Executor;
import ru.spbstu.pipeline.Producer;
import ru.spbstu.pipeline.Status;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class MyWorker implements Executor{
    private final Logger logger; 
    private final Huffman huffman;
    private Producer producer;
    private Consumer consumer;
    private byte[] data;
    private Status status;


    public MyWorker(@NotNull String configFilePath, @NotNull Logger logger) {
        status = Status.OK;
        this.logger = logger;

        Parser parser = new Parser(logger);
        Parameters parameters = parser.ParseConfig(configFilePath);

        if (parser.getStatus()) {
            logger.log("Can't parse file - " + configFilePath);
            status = Status.EXECUTOR_ERROR;
        }

        huffman = new Huffman(parameters, logger);

        if (huffman.getStatus()) {
            logger.log("Some errors in parameters");
            status = Status.EXECUTOR_ERROR;
        }
    }


    @Override
    public void loadDataFrom(@NotNull Producer producer) {
        data = producer.get();
    }


    private void dataProcessing() {

        switch (huffman.getMode()) {
            case ENCODE:
                data = huffman.encode(data).getBytes(StandardCharsets.US_ASCII);

                break;
            case DECODE:
                data = huffman.decode(data).getBytes(StandardCharsets.US_ASCII);

                break;
            default:
                status = Status.EXECUTOR_ERROR;
        }

        if (huffman.getStatus()) {
            logger.log("Error occurred while encoding or decoding of the data");
            status = Status.EXECUTOR_ERROR;
        }

    }


    @Override
    public void run() {
        if (consumer.status() != Status.OK) {
            status = Status.EXECUTOR_ERROR;
        } else {
            data = producer.get();
            dataProcessing();
            consumer.run();
        }

    }

    @Override
    public @NotNull Status status() {
        return status;
    }

    @Override
    public void addProducer(@NotNull Producer producer) {
        this.producer = producer;
    }

    @Override
    public void addProducers(@NotNull List<Producer> list) {

        if (list.size() == 1) {
            addProducer(list.get(0));
        } else {
            logger.log("This worker is supporting only one producer");
            status = Status.EXECUTOR_ERROR;
        }

    }

    @Override
    public void addConsumer(@NotNull Consumer consumer) {
        this.consumer = consumer;
    }

    @Override
    public void addConsumers(@NotNull List<Consumer> list) {

        if (list.size() == 1) {
            addConsumer(list.get(0));
        } else {
            logger.log("This worker is supporting only one consumer");
            status = Status.EXECUTOR_ERROR;
        }

    }

    @Override
    public @NotNull byte[] get() {
        return data.clone();
    }
}
