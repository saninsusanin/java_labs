package com.company.workers;

import com.company.parser.Command;
import com.company.parser.Parameters;
import com.company.parser.Parser;
import org.jetbrains.annotations.NotNull;
import ru.spbstu.pipeline.Producer;
import ru.spbstu.pipeline.Status;
import ru.spbstu.pipeline.Writer;
import ru.spbstu.pipeline.logging.Logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyWriter implements Writer{
    private final static int FIRST_VALUE = 0;

    private final Logger logger;
    private String outputFilePath;
    private FileOutputStream output;
    private Status status;
    private ArrayList<Producer> producers;
    private byte[] data;


    public MyWriter(@NotNull String confFilePath, @NotNull Logger logger)  {
        status = Status.OK;
        this.logger = logger;
        producers = new ArrayList<>();

        Parser parser = new Parser(logger);
        Parameters parameters = parser.ParseConfig(confFilePath);

        outputFilePath = parameters.getParameter(Command.OUTPUT).get(FIRST_VALUE);

        try {
            output = new FileOutputStream(outputFilePath);
            output.close();
            output = new FileOutputStream(outputFilePath, true);
        } catch (FileNotFoundException e) {
            logger.log("File - " + outputFilePath + " not found");
            status = Status.WRITER_ERROR;
        } catch (IOException e) {
            logger.log("File - " + outputFilePath + " can't be closed");
            status = Status.WRITER_ERROR;
        }

    }


    @Override
    public void loadDataFrom(@NotNull Producer producer) {
        data = producer.get();
    }


    @Override
    public void run() {

        try {

            for (Producer producer : producers) {
                loadDataFrom(producer);
                output.write(data);
            }

        } catch (IOException e) {
            logger.log("Can't write data to the file " + outputFilePath);
            status = Status.WRITER_ERROR;
        }

    }


    @Override
    public @NotNull Status status() {
        return status;
    }


    @Override
    public void addProducer(@NotNull Producer producer) {
        producers.add(producer);
    }


    @Override
    public void addProducers(@NotNull List<Producer> list) {

        for (Producer producer : list) {
            addProducer(producer);
        }

    }


    public void close() {

        try {
            output.close();
        } catch (IOException e) {
            logger.log("Can't close file - " + outputFilePath);
        }

    }
}

