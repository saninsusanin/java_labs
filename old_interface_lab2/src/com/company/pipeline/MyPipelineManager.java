package com.company.pipeline;

import com.company.parser.Command;
import com.company.parser.Parameters;
import com.company.parser.Parser;
import com.company.workers.MyReader;
import com.company.workers.MyWriter;
import ru.spbstu.pipeline.Status;
import ru.spbstu.pipeline.logging.Logger;

import java.util.ArrayList;
import java.util.Map;

public class MyPipelineManager implements Runnable{
    private final static int READER_CONFIG = 0;
    private final static int WRITER_CONFIG = 1;
    private final static int FIRST_PIPELINE_CONFIG = 2;

    private Logger logger;


    public MyPipelineManager(Logger logger) {
        this.logger = logger;
    }


    @Override
    public void run() {
        Map<String, String> environmentVariables = System.getenv();

        String pathToConfig = environmentVariables.get("PATH_TO_CONFIG");

        if (pathToConfig == null) {
            logger.log("Set environment variable PATH_TO_CONFIG to the appropriate value");

            return;
        }

        Parser parser = new Parser(logger);
        Parameters parameters = parser.ParseConfig(environmentVariables.get("PATH_TO_CONFIG"));

        //Todo rewrite getting of reader and writer configs
        //reader
        MyReader reader = new MyReader(parameters.getParameter(Command.CONFIGS).get(READER_CONFIG), logger);
        //writer
        MyWriter writer = new MyWriter(parameters.getParameter(Command.CONFIGS).get(WRITER_CONFIG), logger);


        for (int i = FIRST_PIPELINE_CONFIG; i < parameters.getParameter(Command.CONFIGS).size(); ++i) {
            MyPipeline pipeline = new MyPipeline(parameters.getParameter(Command.CONFIGS).get(i), writer, reader, logger);
        }

        while (!reader.isEnded() && reader.getStatus() == Status.OK) {
            //Todo create concurrency
            reader.run();
        }

        reader.close();
        writer.close();
    }
}
