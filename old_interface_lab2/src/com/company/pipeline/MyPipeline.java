package com.company.pipeline;

import com.company.parser.Command;
import com.company.parser.Parameters;
import com.company.parser.Parser;
import com.company.workers.MyReader;
import com.company.workers.MyWriter;
import org.jetbrains.annotations.NotNull;
import ru.spbstu.pipeline.Executor;
import ru.spbstu.pipeline.logging.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class MyPipeline implements Runnable{
    private MyReader reader;


    MyPipeline(@NotNull String pathToConfig, @NotNull MyWriter writer, @NotNull MyReader reader, @NotNull Logger logger) {
        this.reader = reader;

        Parser parser = new Parser(logger);
        Parameters parameters = parser.ParseConfig(pathToConfig);

        if (parser.getStatus()) {
            logger.log("Some went wrong while parsing file - " + pathToConfig);
        }

        ArrayList<String> workers = parameters.getParameter(Command.WORKERS);
        ArrayList<String> configs = parameters.getParameter(Command.CONFIGS);

        if (workers.size() != configs.size()) {
            logger.log("Not enough " + (workers.size() > configs.size() ?  "configs" : "workers"));

            return;
        }

        ArrayList<Executor> classes = new ArrayList<>();

        try {

            if (workers.size() == 0) {
                reader.addConsumer(writer);
                writer.addProducer(reader);
            } else {

                for (int i = 0; i < workers.size(); ++i) {
                    Class<?> current = Class.forName(workers.get(i));
                    Executor executor = (Executor) current.getConstructor(String.class, Logger.class).newInstance(configs.get(i), logger);
                    classes.add(executor);
                }

                reader.addConsumer(classes.get(0));

                for (int i = 0; i < workers.size(); ++i) {
                    Executor executor = classes.get(i);
                    executor.addProducer(i == 0 ? reader : classes.get(i - 1));
                    executor.addConsumer(i == workers.size() - 1 ? writer : classes.get(i + 1));
                }

                writer.addProducer(classes.get(workers.size() - 1));
            }

        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            logger.log(e.getMessage());
        }

    }


    @Override
    public void run() {
        reader.run();
    }
}
