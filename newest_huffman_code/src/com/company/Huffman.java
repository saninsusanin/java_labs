package com.company;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Huffman {
    private final String input;
    private final String output;
    private final String codes;
    private Map<Character, Integer> frequencies;
    private Map<String, Character> codesCharacters;
    private Map<Character, String> charactersCodes;
    private final static int eof;
    private final static int bin;
    private final static String FALSE;
    private final static String TRUE;
    private final static String EMPTY_STRING;
    private final static char separator;
    private final static char enter;
    private boolean isErrorOccurred;

    static {
        eof = -1;
        bin = 2;
        FALSE = "0";
        TRUE = "1";
        EMPTY_STRING = "";
        separator = '=';
        enter = '\n';
    }

    {
        frequencies = new HashMap<>();
        codesCharacters = new HashMap<>();
        charactersCodes = new HashMap<>();
        isErrorOccurred = false;
    }

    Huffman(Parameters parameters) {
        input = parameters.getPath(Command.INPUT);
        output = parameters.getPath(Command.OUTPUT);
        codes = parameters.getPath(Command.CODES);
        Mode mode = parameters.getMode();

        if (input.length() == 0) {
            Logger.write("Set path to the input file in the config file");
            isErrorOccurred = true;
        }

        if (output.length() == 0) {
            Logger.write("Set path to the output file in the config file");
            isErrorOccurred = true;
        }

        switch (mode) {
            case NONE:
                Logger.write("You have to set parameter mode in the config file");
                isErrorOccurred = true;

                break;
            case DECODE:

                if (codes.length() == 0) {
                    Logger.write("Set path to the codes file in the config file");
                    isErrorOccurred = true;
                } else {

                    if (!isErrorOccurred) {
                        decode();
                    }

                }

                break;
            case ENCODE:

                if (!isErrorOccurred) {
                    encode();
                }

                break;
        }
    }

    private static class Node implements Comparable<Node> {
        final Integer sum;
        final Character character;
        final Node left;
        final Node right;

        public Node(Character character, Integer sum, Node left, Node right) {
            this.sum = sum;
            this.character = character;
            this.left = left;
            this.right = right;
        }

        @Override
        public int compareTo(Node rightOperand) {
            return Integer.compare(sum, rightOperand.sum);
        }

    }

    private void getFrequencies() {
        BufferedReader input;

        try {
            input = new BufferedReader(new FileReader(new File(this.input)));

            int character;

            while ((character = input.read()) != eof) {

                if (!frequencies.containsKey((char) character)) {
                    frequencies.put((char) character, 0);
                }

                frequencies.put((char)character, frequencies.get((char)character) + 1);
            }

            input.close();
        } catch (IOException e) {
            Logger.write("Maybe file " + this.input + " isn't exists. Or maybe it isn't closed");
            isErrorOccurred = true;
        }

    }

    private void setCodes(Node currentNode, String currentCode) {

        if (currentNode.left == null && currentNode.right == null) {
            codesCharacters.put(currentCode, currentNode.character);
            charactersCodes.put(currentNode.character, currentCode);
        }

        if (currentNode.left != null) {
            setCodes(currentNode.left, currentCode + FALSE);
        }

        if (currentNode.right != null) {
            setCodes(currentNode.right, currentCode + TRUE);
        }

    }

    private void fillCodes() {
        BufferedWriter codes;

        try {
            codes = new BufferedWriter(new FileWriter(new File(this.codes)));

            for (Map.Entry<Character, String> entry : charactersCodes.entrySet()) {
                codes.write(entry.getKey());
                codes.write(separator);
                codes.write(entry.getValue());
                codes.write(enter);
            }

            codes.close();
        }
        catch(IOException e) {
            Logger.write("Something went wrong with " + this.codes);
            isErrorOccurred = true;
        }

    }

    private void encode() {
        getFrequencies();
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();

        for (Map.Entry<Character, Integer> entry : frequencies.entrySet()) {
            priorityQueue.add(new Node(entry.getKey(), entry.getValue(), null, null));
        }

        while (priorityQueue.size() > 1) {
            Node firstNode = priorityQueue.poll();
            Node secondNode = priorityQueue.poll();
            priorityQueue.add(new Node('\0', firstNode.sum + secondNode.sum, firstNode, secondNode));
        }

        Node root = priorityQueue.poll();

        if (root != null) {

            if (root.left == null && root.right == null) {
                codesCharacters.put(FALSE, root.character);
                charactersCodes.put(root.character, FALSE);
            } else {
                setCodes(root, EMPTY_STRING);
                fillCodes();
            }

        }

        BufferedReader input;
        BufferedWriter output;

        try {
            input = new BufferedReader(new FileReader(new File(this.input)));
            output = new BufferedWriter(new FileWriter(new File(this.output)));

            int character;

            while ((character = input.read()) != eof) {
                output.write(Integer.parseInt(charactersCodes.get((char)character), bin));
            }

            input.close();
            output.close();
        } catch(IOException e) {
            Logger.write("Something went wrong with " + this.input + " or " + this.output);
            isErrorOccurred = true;
        }

    }

    private void getCodes() throws IOException {
        char symbol;
        int character;
        BufferedReader codes  = new BufferedReader( new FileReader(new File(this.codes)));

        while ((character = codes.read()) != eof) {
            symbol = (char)character;
            codes.read();
            StringBuilder code = new StringBuilder();

            while ((character = codes.read()) != enter && character != eof) {
                code.append((char)character);
            }

            charactersCodes.put(symbol, code.toString());
            codesCharacters.put(code.toString(), symbol);
        }
    }

    private void decode() {
        BufferedReader input;
        BufferedWriter output;

        try {
            input = new BufferedReader(new FileReader(new File(this.input)));
            output = new BufferedWriter(new FileWriter(new File(this.output)));

            getCodes();

            int character;

            while ((character = input.read()) != eof) {
                output.write(codesCharacters.get(Integer.toBinaryString(character)));
            }

            input.close();
            output.close();
        } catch(IOException e) {
            Logger.write("Something went wrong with " + this.input + " or " + this.output);
            isErrorOccurred = true;
        }

    }

    public boolean getStatus() {
        return isErrorOccurred;
    }
}
