package com.company;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class Logger {
    private final static String logger;
    private final static String enter;
    private static BufferedWriter input;

    static {
        Map<String, String> environmentVariables = System.getenv();
        logger = environmentVariables.get("PATH_TO_LOGGER");
        enter = "\n";
    }

    static public void initialize() {
        try {
            input = new BufferedWriter(new FileWriter(new File(logger)));
            input.close();
            input = new BufferedWriter(new FileWriter(new File(logger), true));
        } catch (IOException e) {
            System.out.println("Can't find the file " + logger);
        }
    }

    static public void close() {
        try {
            input.close();
        } catch (IOException e) {
            System.out.println("Can't close the file " + logger);
        }
    }

    static public void write(String message) {
        try {
            input.write(message + enter);
        } catch (IOException e) {
            System.out.println("Something went wrong with file" + logger);
        }

    }
}
