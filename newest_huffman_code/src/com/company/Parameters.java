package com.company;

import java.util.HashMap;

public class Parameters {
    private HashMap<Command, String> paths;
    private Mode mode;

    {
        paths = new HashMap<>();
        paths.put(Command.INPUT, "");
        paths.put(Command.OUTPUT, "");
        paths.put(Command.CODES, "");

        mode = Mode.NONE;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public void setPath(Command command, String path) {
        paths.put(command, path);
    }

    public String getPath(Command command) {
        return paths.get(command);
    }

    public Mode getMode() {
        return mode;
    }
}
