package com.company;

public enum Mode {
    NONE,
    ENCODE,
    DECODE
}
