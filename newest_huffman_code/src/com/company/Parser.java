package com.company;

import java.io.*;
import java.util.HashMap;

public class Parser {
    private final static char separator;
    private final static char comment;
    private final static char space;
    private final static char enter;
    private final static int eof;
    private final static HashMap<String, Command> commands;
    private final static HashMap<String, Mode> modes;
    private static boolean fileIsEnded;
    private boolean isErrorOccurred = false;

    static {
        separator = '=';
        comment = '#';
        space = ' ';
        enter = '\n';
        eof = -1;

        modes = new HashMap<>();
        modes.put("ENCODE", Mode.ENCODE);
        modes.put("DECODE", Mode.DECODE);

        commands = new HashMap<>();
        commands.put("INPUT_PATH", Command.INPUT);
        commands.put("OUTPUT_PATH", Command.OUTPUT);
        commands.put("CODES_PATH", Command.CODES);
        commands.put("MODE", Command.MODE);

        fileIsEnded = false;
    }

    static class DataFromLine {
        private String parameterName;
        private String parameterValue;

        public void setParameterName(String parameterName) {
            this.parameterName = parameterName;
        }

        public void setParameterValue(String parameterValue) {
            this.parameterValue = parameterValue;
        }

        public String getParameterName() {
            return parameterName;
        }

        public String getParameterValue() {
            return parameterValue;
        }
    }

    private int skipAllWhitespaces(BufferedReader input, int currentCharacter) throws IOException {

        while ((currentCharacter == enter || currentCharacter == space) && currentCharacter != eof) {
            currentCharacter = input.read();
        }

        return currentCharacter;
    }

    private int getEssential(StringBuilder currentString, BufferedReader input, int currentCharacter) throws IOException {

        while (currentCharacter != separator && currentCharacter != eof &&
                currentCharacter != enter && currentCharacter != space) {
            currentString.append((char) currentCharacter);
            currentCharacter = input.read();
        }

        return currentCharacter;
    }

    private DataFromLine lineParser(BufferedReader input, String pathToConfig) throws IOException{
        int character;

        StringBuilder parameterName = new StringBuilder();
        StringBuilder parameterValue = new StringBuilder();
        DataFromLine data = new DataFromLine();

        character = input.read();
        character = skipAllWhitespaces(input, character);

        if (character == comment) {
            data.setParameterName(new String(parameterName));
            data.setParameterValue(new String(parameterValue));

            while (true) {
                character = input.read();

                if (character == eof) {
                    fileIsEnded = true;
                    return data;
                } else if(character == enter){
                    return data;
                }

            }

        } else {
            character = getEssential(parameterName, input, character);
            character = skipAllWhitespaces(input, character);

            if (character != separator) {
                Logger.write("Error in file " + pathToConfig + ". You have to put = between command name and command value");
                isErrorOccurred = true;

                while (input.read() != '\n');

                data.setParameterName(new String(parameterName));
                data.setParameterValue(new String(parameterValue));

                return data;
            }

            character = input.read();
            character = skipAllWhitespaces(input, character);
            character = getEssential(parameterValue, input, character);

            if (character == eof) {
                fileIsEnded = true;
            }
        }

        data.setParameterName(new String(parameterName));
        data.setParameterValue(new String(parameterValue));
        return data;
    }

    public Parameters parseConfig(String pathToConfig) {
        BufferedReader config;
        Parameters parameters = new Parameters();

        try {
            config = new BufferedReader(new FileReader(new File(pathToConfig)));

            while (!fileIsEnded) {
                DataFromLine data = lineParser(config, pathToConfig);

                if (data.parameterName.length() == 0 || data.getParameterValue().length() == 0) {

                    continue;
                }

                Command current_command = commands.get(data.parameterName);

                if (current_command == null) {
                    Logger.write("No such command - " + data.getParameterName());
                    isErrorOccurred = true;

                    break;
                }

                if (current_command == Command.MODE){
                    Mode current_mode = modes.get(data.parameterValue);

                    if (current_mode == null) {
                        Logger.write("No such mode -  " + data.getParameterValue());
                        isErrorOccurred = true;

                        break;
                    } else {

                        if (parameters.getMode() == Mode.NONE) {
                            parameters.setMode(current_mode);

                            continue;
                        } else {
                            Logger.write("Mode is already set to " + data.getParameterValue());
                            isErrorOccurred = true;

                            break;
                        }

                    }

                }

                if (parameters.getPath(current_command).length() == 0) {
                    parameters.setPath(current_command, data.parameterValue);
                } else {
                    Logger.write("Path to " + data.getParameterName() + " is already set to " + parameters.getPath(current_command));
                    isErrorOccurred = true;

                    break;
                }

            }

            config.close();
        } catch (IOException e) {
            Logger.write("Something went wrong with file " + pathToConfig);
            isErrorOccurred = true;
        }

        return parameters;
    }

    public boolean getStatus() {
        return isErrorOccurred;
    }
}
