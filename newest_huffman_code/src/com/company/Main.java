package com.company;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, String> environmentVariables = System.getenv();

        Logger.initialize();
        Parser parser = new Parser();
	    Parameters parameters = parser.parseConfig(environmentVariables.get("PATH_TO_CONFIG"));

	    if (!parser.getStatus()) {
	        Logger.write("Config was parsed and parameters were successfully received");

            Huffman huffman = new Huffman(parameters);

            if (!huffman.getStatus()) {
                Logger.write("Successfully " + parameters.getMode().toString().toLowerCase() + "d");
            }
        }

	    Logger.close();
    }
}
