package spbstu.viatkin.workers;

import spbstu.viatkin.parser.Command;
import spbstu.viatkin.parser.Parameters;
import spbstu.viatkin.parser.Parser;
import org.jetbrains.annotations.NotNull;
import ru.spbstu.pipeline.Consumer;
import ru.spbstu.pipeline.Producer;
import ru.spbstu.pipeline.Reader;
import ru.spbstu.pipeline.Status;
import ru.spbstu.pipeline.logging.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyReader implements Reader {
    private final Logger logger;
    private int bufferSize;
    private String inputFilePath;
    private boolean isEnded;
    private byte[] data;
    private InputStream input;
    private ArrayList<Consumer> consumers;
    private Status status;
    private Set<String> outputDataTypes;

    private final static int FIRST_VALUE = 0;
    private final static int dec = 10;
    private final static int eof = -1;


    public MyReader(@NotNull String confFilePath, @NotNull Logger logger)  {
        this.logger = logger;
        consumers = new ArrayList<>();
        status = Status.OK;
        outputDataTypes = new HashSet<>();

        outputDataTypes.add(byte[].class.getCanonicalName());
        outputDataTypes.add(char[].class.getCanonicalName());
        outputDataTypes.add(String.class.getCanonicalName());
        
        Parser parser = new Parser(logger);
        Parameters parameters = parser.ParseConfig(confFilePath);
        inputFilePath = parameters.getParameter(Command.INPUT).get(FIRST_VALUE);
        String bufferSizeString = parameters.getParameter(Command.BUFFER_SIZE).get(FIRST_VALUE);

        try {
            bufferSize = Integer.parseInt(bufferSizeString, dec);
        } catch (NumberFormatException e) {
            logger.log("Can't be parsed buffer size which is set to - " + bufferSizeString + " Buffer size has to be a even number");
            status = Status.READER_ERROR;
        }

        data = new byte[bufferSize];

        try {
            input = new FileInputStream(inputFilePath);
            //for skipping BOM symbol
            byte[] tmp = new byte[2];
            input.read(tmp);
        } catch (FileNotFoundException e) {
            logger.log("Can't open file " + inputFilePath);
            status = Status.READER_ERROR;
        } catch (IOException e) {
            logger.log("Can't read BOM symbol " + inputFilePath);
            status = Status.READER_ERROR;
        }

    }


    public void read() {
        try {
            int size = input.read(data);

            if (size == eof) {
                isEnded = true;
            } else if (size != bufferSize) {
                byte[] temporary = new byte[size];
                System.arraycopy(data, 0, temporary, 0, size);
                data = temporary;
            }

        } catch (FileNotFoundException e) {
            logger.log("File - " + inputFilePath + " doesn't exist or can't be opened for reading");
            status = Status.READER_ERROR;
        } catch (IOException e) {
            logger.log("Can't read data from file - " + inputFilePath);
            status = Status.READER_ERROR;
        }
    }


    public @NotNull byte[] get() {
        return data.clone();
    }


    public boolean isEnded() {
        return isEnded;
    }


    public void close() {

        try {
            input.close();
        } catch (IOException e) {
            logger.log("Can't close file " + inputFilePath);
        }

    }


    @Override
    public void run() {
        read();

        while (!isEnded) {

            for (Consumer consumer : consumers) {

                if (consumer.loadDataFrom(this) == 0) {
                    status = Status.READER_ERROR;

                    return;
                }

                consumer.run();
                this.status = consumer.status();

                if (this.status != Status.OK) {
                    return;
                }
            }

            read();
        }

    }


    @Override
    public void addConsumer(@NotNull Consumer consumer) {
        consumers.add(consumer);
    }


    @Override
    public void addConsumers(@NotNull List<Consumer> list) {

        for (Consumer consumer : list) {
            addConsumer(consumer);
        }

    }


    public Status getStatus() {
        return status;
    }


    public class StringAccessor implements Producer.DataAccessor {
        private String convertFromBytesToString(byte[] data) {
            return new String(data, StandardCharsets.UTF_16BE);
        }

        @Override
        public @NotNull Object get() {
            return convertFromBytesToString(data);
        }

        @Override
        public long size() {
            return convertFromBytesToString(data).length();
        }
    }

    public class ByteAccessor implements Producer.DataAccessor {
        @Override
        public @NotNull Object get() {
            return data.clone();
        }

        @Override
        public long size() {
            return data.length;
        }
    }

    public class CharAccessor implements Producer.DataAccessor {
        private char[] fromBytesToChars(byte[] data) {
            return new String(data, StandardCharsets.UTF_16BE).toCharArray();
        }

        @Override
        public @NotNull Object get() {
            return fromBytesToChars(data);
        }

        @Override
        public long size() {
            return fromBytesToChars(data).length;
        }
    }

    @Override
    public @NotNull MyReader.DataAccessor getAccessor(@NotNull String s) {
        if (s.equals(String.class.getCanonicalName()))
            return new StringAccessor();
        else if (s.equals(byte[].class.getCanonicalName()))
            return new ByteAccessor();
        else if (s.equals(char[].class.getCanonicalName()))
            return new CharAccessor();
        else
            status = Status.READER_ERROR;

            return new ByteAccessor();
    }


    @Override
    public @NotNull Set<String> outputDataTypes() {
        return outputDataTypes;
    }
}
