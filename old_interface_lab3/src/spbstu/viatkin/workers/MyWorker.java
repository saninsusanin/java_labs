package spbstu.viatkin.workers;

import spbstu.viatkin.coder.Huffman;
import ru.spbstu.pipeline.logging.Logger;
import spbstu.viatkin.parser.Parameters;
import spbstu.viatkin.parser.Parser;
import org.jetbrains.annotations.NotNull;
import ru.spbstu.pipeline.Consumer;
import ru.spbstu.pipeline.Executor;
import ru.spbstu.pipeline.Producer;
import ru.spbstu.pipeline.Status;

import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyWorker implements Executor{
    private final Logger logger; 
    private final Huffman huffman;
    private Consumer consumer;
    private String data;
    private Status status;
    private Set<String> outputDataTypes;
    private Producer.DataAccessor dataAccessor;


    public MyWorker(@NotNull String configFilePath, @NotNull Logger logger) {
        status = Status.OK;
        this.logger = logger;
        outputDataTypes = new HashSet<>();

        outputDataTypes.add(String.class.getCanonicalName());
        outputDataTypes.add(byte[].class.getCanonicalName());
        outputDataTypes.add(char[].class.getCanonicalName());

        Parser parser = new Parser(logger);
        Parameters parameters = parser.ParseConfig(configFilePath);

        if (parser.getStatus()) {
            logger.log("Can't parse file - " + configFilePath);
            status = Status.EXECUTOR_ERROR;
        }

        huffman = new Huffman(parameters, logger);

        if (huffman.getStatus()) {
            logger.log("Some errors in parameters");
            status = Status.EXECUTOR_ERROR;
        }
    }


    @Override
    public long loadDataFrom(@NotNull Producer producer) {
        data = (String)dataAccessor.get();

        return data.length();
    }


    private void dataProcessing() {

        switch (huffman.getMode()) {
            case ENCODE:
                data = huffman.encode(data.getBytes(StandardCharsets.UTF_16BE));

                break;
            case DECODE:
                data = huffman.decode(data.getBytes(StandardCharsets.US_ASCII));

                break;
            default:
                status = Status.EXECUTOR_ERROR;
        }

        if (huffman.getStatus()) {
            logger.log("Error occurred while encoding or decoding of the data");
            status = Status.EXECUTOR_ERROR;
        }

    }


    @Override
    public void run() {
        if (consumer.status() != Status.OK) {
            status = Status.EXECUTOR_ERROR;
        } else {
            dataProcessing();

            if (consumer.loadDataFrom(this) == 0) {
                status = Status.READER_ERROR;
                return;
            }

            consumer.run();
            this.status = consumer.status();
        }

    }


    @Override
    public @NotNull Status status() {
        return status;
    }


    private static byte[] converterFromStringToBytes(String string) {
        return string.getBytes(StandardCharsets.US_ASCII).clone();
    }

    private static char[] converterFromStringToChars(String string) {
        return string.toCharArray();
    }


    @Override
    public void addProducer(@NotNull Producer producer) {
        Set<String> outputFormats = producer.outputDataTypes();

        if (!outputFormats.contains(String.class.getCanonicalName()))
            status = Status.ERROR;

        dataAccessor = producer.getAccessor(String.class.getCanonicalName());
    }


    @Override
    public void addProducers(@NotNull List<Producer> list) {

        if (list.size() == 1) {
            addProducer(list.get(0));
        } else {
            logger.log("This worker is supporting only one producer");
            status = Status.EXECUTOR_ERROR;
        }

    }


    @Override
    public void addConsumer(@NotNull Consumer consumer) {
        this.consumer = consumer;
    }


    @Override
    public void addConsumers(@NotNull List<Consumer> list) {

        if (list.size() == 1) {
            addConsumer(list.get(0));
        } else {
            logger.log("This worker is supporting only one consumer");
            status = Status.EXECUTOR_ERROR;
        }

    }


    public class StringAccessor implements Producer.DataAccessor {
        @Override
        public @NotNull Object get() {
            return data;
        }

        @Override
        public long size() {
            return data.length();
        }
    }

    public class ByteAccessor implements Producer.DataAccessor {
        @Override
        public @NotNull Object get() {
            return converterFromStringToBytes(data);
        }

        @Override
        public long size() {
            return converterFromStringToBytes(data).length;
        }
    }

    public class CharAccessor implements Producer.DataAccessor {
        @Override
        public @NotNull Object get() {
            return converterFromStringToChars(data);
        }

        @Override
        public long size() {
            return converterFromStringToChars(data).length;
        }
    }


    @Override
    public @NotNull MyWorker.DataAccessor getAccessor(@NotNull String outputDataType) {

        if (outputDataType.equals(String.class.getCanonicalName())) {
            return new StringAccessor();
        } else if (outputDataType.equals(byte[].class.getCanonicalName())) {
            return new ByteAccessor();
        } else if (outputDataType.equals(char[].class.getCanonicalName())) {
            return new CharAccessor();
        } else {
            status = Status.EXECUTOR_ERROR;

            return new StringAccessor();
        }

    }


    @Override
    public @NotNull Set<String> outputDataTypes() {
        return outputDataTypes;
    }
}
