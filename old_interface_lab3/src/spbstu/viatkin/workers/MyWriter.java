package spbstu.viatkin.workers;

import spbstu.viatkin.parser.Command;
import spbstu.viatkin.parser.Parameters;
import spbstu.viatkin.parser.Parser;
import org.jetbrains.annotations.NotNull;
import ru.spbstu.pipeline.Producer;
import ru.spbstu.pipeline.Status;
import ru.spbstu.pipeline.Writer;
import ru.spbstu.pipeline.logging.Logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class MyWriter implements Writer{
    private final static int FIRST_VALUE = 0;
    private final static byte[] BOM = {(byte)0xfe, (byte)0xff};

    private final Logger logger;
    private String outputFilePath;
    private FileOutputStream output;
    private Status status;
    private ArrayList<Producer.DataAccessor> dataAccessors;
    private byte[] data;


    public MyWriter(@NotNull String confFilePath, @NotNull Logger logger)  {
        status = Status.OK;
        this.logger = logger;
        dataAccessors = new ArrayList<>();

        Parser parser = new Parser(logger);
        Parameters parameters = parser.ParseConfig(confFilePath);

        outputFilePath = parameters.getParameter(Command.OUTPUT).get(FIRST_VALUE);

        try {
            output = new FileOutputStream(outputFilePath);
            output.close();
            output = new FileOutputStream(outputFilePath, true);
            output.write(BOM);
        } catch (FileNotFoundException e) {
            logger.log("File - " + outputFilePath + " not found");
            status = Status.WRITER_ERROR;
        } catch (IOException e) {
            logger.log("File - " + outputFilePath + " can't be closed");
            status = Status.WRITER_ERROR;
        }

    }


    @Override
    public long loadDataFrom(@NotNull Producer producer) {
        data = (byte[]) producer.getAccessor(byte[].class.getCanonicalName()).get();

        return data.length;
    }


    @Override
    public void run() {

        try {

            for (Producer.DataAccessor dataAccessor : dataAccessors) {
                data = (byte[]) dataAccessor.get();
                output.write(data);
            }

        } catch (IOException e) {
            logger.log("Can't write data to the file " + outputFilePath);
            status = Status.WRITER_ERROR;
        }

    }


    @Override
    public @NotNull Status status() {
        return status;
    }


    @Override
    public void addProducer(@NotNull Producer producer) {
        Set<String> outputFormats = producer.outputDataTypes();

        if (!outputFormats.contains(byte[].class.getCanonicalName()))
            status = Status.WRITER_ERROR;

        dataAccessors.add(producer.getAccessor(byte[].class.getCanonicalName()));
    }


    @Override
    public void addProducers(@NotNull List<Producer> list) {

        for (Producer producer : list) {
            addProducer(producer);
        }

    }


    public void close() {

        try {
            output.close();
        } catch (IOException e) {
            logger.log("Can't close file - " + outputFilePath);
        }

    }
}

