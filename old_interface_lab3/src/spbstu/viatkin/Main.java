package spbstu.viatkin;

import spbstu.viatkin.pipeline.MyPipelineManager;
import ru.spbstu.pipeline.logging.UtilLogger;

import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {

    public static void main(String[] args) {
        Map<String, String> environmentVariables = System.getenv();
        String pathToLogger = environmentVariables.get("PATH_TO_LOG");

        if (pathToLogger == null) {
            System.out.println("Set environment variable PATH_TO_LOG to appropriate value");

            return;
        }

        Logger logger = Logger.getLogger("Logger");
        logger.setUseParentHandlers(false);
        try {
            FileHandler fileHandler = new FileHandler(pathToLogger);
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
        }
        catch (Exception ex) {
            System.out.println("Error while opening log file");

            return;
        }

        UtilLogger utilLogger = UtilLogger.of(logger);
        MyPipelineManager pipelineManager = new MyPipelineManager(utilLogger);
        pipelineManager.run();
    }
}
