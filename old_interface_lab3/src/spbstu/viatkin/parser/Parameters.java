package spbstu.viatkin.parser;

import java.util.ArrayList;
import java.util.HashMap;

public class Parameters {
    private HashMap<Command, ArrayList<String>> map = new HashMap<>();


    private void add(Command key, String value) {
        ArrayList<String> list = this.map.computeIfAbsent(key, k -> new ArrayList<>());

        list.add(value);
    }


    public void setParameter(Command command, String parameter) { add(command, parameter); }


    public ArrayList<String> getParameter(Command command) {
        return map.get(command);
    }
}
